<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BasicModel extends Model
{
    /**
     * change the value of created_at attribute.
     *
     * @param string $value
     * @return mixed
     */
    public function getCreatedAtAttribute($value): mixed
    {
        if (empty($value)) {
            return null;
        }

        return date("Y/m/d H:i", strtotime($value));
    }

    /**
     * change the value of created_at attribute.
     *
     * @param string $value
     * @return mixed
     */
    public function getUpdatedAtAttribute($value): mixed
    {
        if (empty($value)) {
            return null;
        }

        return date("Y/m/d H:i", strtotime($value));
    }
}
