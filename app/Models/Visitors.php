<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Visitors extends BasicModel
{
    use HasFactory;

    protected const INACTIVE = 0;
    protected const ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['exhibition_id', 'name', 'email', 'status'];

    /**
     * scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder  $query
     * @return void
     */
    public function scopeActive($query)
    {
        $query->where('status', Visitors::ACTIVE);
    }
}
