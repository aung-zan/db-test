<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ExhibitionItems extends BasicModel
{
    use HasFactory;
}
