<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Exhibitions extends BasicModel
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'exhibition_id';
    }

    public function exhibitionItems()
    {
        return $this->hasMany(ExhibitionItems::class, 'exhibition_id', 'exhibition_id');
    }
}
