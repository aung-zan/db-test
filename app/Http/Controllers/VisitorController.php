<?php

namespace App\Http\Controllers;

use App\Repositories\VisitorRepository;
use App\Services\GenerateHtmlService;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    private $visitorRepository;

    public function __construct(VisitorRepository $visitorRepository)
    {
        $this->visitorRepository = $visitorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($exhibition_id)
    {
        $activeVisitors = $this->visitorRepository
            ->activeVisitors($exhibition_id)
            // ->visitors($exhibition_id)
            ->get();

        $visitors = $this->visitorRepository
            ->visitorsWithProperties($exhibition_id)
            ->get();

        return view('visitors.show', [
            'visitors' => $visitors,
            'activeVisitors' => $activeVisitors,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Generate html file with codes.
     *
     * @return void
     */
    public function generateHtml($exhibition_id)
    {
        $visitors = $this->visitorRepository
            ->visitorsWithProperties($exhibition_id)
            ->get();

        $generateHtmlService = new GenerateHtmlService();
        $generateHtmlService->generateHtml($exhibition_id, $visitors);
    }
}
