<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class GenerateHtmlService
{
    private $defaultHtmlPath;
    private $defaultHtml;

    public function __construct()
    {
        $this->defaultHtmlPath = storage_path('app/document/html/default/default.html');
        $this->defaultHtml = file_get_contents($this->defaultHtmlPath);
    }

    /**
     * generate new html file.
     *
     * @param int $exhibition_id
     * @param object $data
     * @return void
     */
    public function generateHtml($exhibition_id, $data): void
    {
        $content = $this->createContent($data);

        $htmlFileName = $exhibition_id . '.html';

        Storage::put('document/html/' . $exhibition_id . '/' . $htmlFileName, $content);
    }

    /**
     * create content string.
     *
     * @param object $data
     * @return string $content
     */
    private function createContent($data): string
    {
        $content = $this->defaultHtml;

        foreach ($data as $value) {
            $visitorData = '<div>
                <label>Name: <label>
                <label>' . $value->name . '</label></br>
                <label>Gender: </label>
                <label>' . $value . '</label></br>
            </div>';

            $content .= $visitorData;
        }
        $content .= '</body></html>';

        return $content;
    }
}
