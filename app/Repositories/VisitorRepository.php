<?php

namespace App\Repositories;

use App\Models\Visitors;
use Illuminate\Support\Facades\DB;

class VisitorRepository
{
    /**
     * a query to get all visitors by exhibition_id.
     *
     * @param int $exhibition_id
     * @return \Illuminate\Database\Eloquent\Builder  $query
     */
    public function visitors($exhibition_id): mixed
    {
        return Visitors::where('exhibition_id', $exhibition_id);
    }

    /**
     * a query to get all active visitors by exhibition_id.
     *
     * @param int $exhibition_id
     * @return \Illuminate\Database\Eloquent\Builder  $query
     */
    public function activeVisitors($exhibition_id): mixed
    {
        return $this->visitors($exhibition_id)
            ->Active();
    }

    /**
     * a query to get all visitors with their properites by exhibition_id.
     *
     * @param int $exhibition_id
     * @return \Illuminate\Database\Query\Builder  $query
     */
    public function visitorsWithProperties($exhibition_id): mixed
    {
        $property = $this->property();
        $stringAgg = "properties.property";

        return $this->dbVisitors($exhibition_id)
            ->leftJoinSub(
                $property,
                'properties',
                'properties.visitor_id',
                '=',
                'visitors.visitor_id'
            )
            ->groupBy('visitors.visitor_id')
            ->select('name', DB::raw("STRING_AGG(" . $stringAgg . ", ',') as visitors_property"));
    }

    /**
     * a query to get all visitors by exhibition_id.
     *
     * @param int $exhibition_id
     * @return \Illuminate\Database\Query\Builder  $query
     */
    private function dbVisitors($exhibition_id): mixed
    {
        return DB::table('visitors')->where('exhibition_id', $exhibition_id);
    }

    /**
     * a subquery to get exhibition_items and visitor_items.
     *
     * @return \Illuminate\Database\Query\Builder  $query
     */
    private function property(): mixed
    {
        return DB::table('exhibition_items')
            ->leftJoin('visitor_items', 'visitor_items.exhibition_item_id', '=', 'exhibition_items.exhibition_item_id')
            ->orderBy('visitor_items_id')
            ->orderBy('exhibition_items.exhibition_item_id')
            ->select('visitor_id', DB::raw("CONCAT(label, ':', value) as property"));
    }
}
