<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitorItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitor_items', function (Blueprint $table) {
            $table->id('visitor_items_id');
            $table->foreignId('exhibition_item_id')
                ->constrained('exhibition_items', 'exhibition_item_id')
                ->onDelete('cascade');
            $table->foreignId('visitor_id')
                ->constrained('visitors', 'visitor_id')
                ->onDelete('cascade');
            $table->string('value', 100);
            $table->timestamps();
            $table->softDeletes($column = 'deleted_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitor_items');
    }
}
