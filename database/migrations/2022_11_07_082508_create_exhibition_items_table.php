<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExhibitionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibition_items', function (Blueprint $table) {
            $table->id('exhibition_item_id');
            $table->foreignId('exhibition_id')
                ->constrained('exhibitions', 'exhibition_id')
                ->onDelete('cascade');
            $table->string('label', 100);
            $table->timestamps();
            $table->softDeletes($column = 'deleted_at', $precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibition_items');
    }
}
