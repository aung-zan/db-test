<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('exhibitions/{exhibition}')->group(function () {
    Route::controller(ExhibitionController::class)->group(function () {
        Route::get('', 'show')->name('get_exhibition');
    });

    Route::controller(ExhibitionItemController::class)->group(function () {
        Route::get('exhibition_items', 'index')->name('get_exhibition_items');
    });

    Route::prefix('visitors')
        ->controller(VisitorController::class)
        ->group(function () {
            Route::get('', 'index')->name('get_visitors');
            Route::get('generate', 'generateHtml')->name('generate_html');
        });
});
